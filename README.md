# LWJGL 2D Engine
A 2D game engine written in Java using LWJGL.

Libraries:
* LWJGL
* GLFW
* OpenGL
* OpenAL
* JOML
* STB

[Example game](https://gitlab.com/ZephaniahNoah/lwjgl-2d-engine/-/tree/main/src/example/breakout) using this engine:

![Breakout](screenshots/breakout.png)
package example.spaceinvaders;

import java.awt.Color;

import org.joml.Vector2d;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Log;
import com.zephaniahnoah.lwjgl2D.Sound;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class EnemyShip extends Ship implements Log {

	public static Texture texture;
	private boolean wander = false;
	private int timer = 0;
	private float wanderDirection = 0;
	public int speed = 7;
	public int secondShot = 0;
	public Sound shootSound = new Sound("pew");

	public EnemyShip() {
		solid = false;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		SpaceInvaders g = SpaceInvaders.instance;

		if (wander) {
			timer++;

			if (position.x < 0) {
				position.x = 0;
				wanderDirection *= -1;
			}
			if (position.x > g.window.width) {
				position.x = g.window.width;
				wanderDirection *= -1;
			}

			velocity.x = (wanderDirection * speed) * delta;
		} else {
			double dif = position.x - g.player.position.x;

			velocity.x = ((float) (Math.signum(dif) * -1) * speed) * delta;

			double playerX = g.player.position.x;

			// If enemy ship is aligned with the player
			if (position.x > playerX - 15 && position.x < playerX + 15 && position.y - 15d < g.window.height) {

				shootSound.play();

				for (int i = -1; i < 2; i++) {
					Bullet bullet;
					g.world.addEntity(bullet = new Bullet(this));
					bullet.direction = -1;
					bullet.initPos(new Vector2d(g.player.position.x + i * 20, position.y - 15d));
					bullet.color = Color.RED;
					bullet.velocity.x = i * 2f;
				}

				wander = true;
				wanderDirection = SpaceInvaders.rand.nextInt(2) == 1 ? -1 : 1;
			}
		}

		if (timer > 80) {
			wander = false;
			timer = 0;
		}

		if (position.y < 0 - size.y) {
			delete = true;
		}

		velocity.y = -3 * delta;
	}

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		super.draw(delta, lerpX, lerpY, game);
		drawHP(game.texturedShader, lerpX, (int) (lerpY - (getTexture().height - 55)), 100, 4);
	}

	@Override
	public Texture getTexture() {
		return texture;
	}
}
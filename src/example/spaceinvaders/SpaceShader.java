package example.spaceinvaders;

import com.zephaniahnoah.lwjgl2D.gfx.shaders.TexturedShader;

public class SpaceShader extends TexturedShader {

	private int location_iTime;

	public SpaceShader() {
		super("space");
	}

	public void loadTime(float time) {
		super.loadFloat(location_iTime, time);
	}

	@Override
	protected void getAllUniformLocations() {
		super.getAllUniformLocations();
		location_iTime = super.getUniformLocation("iTime");
	}
}
package example.spaceinvaders;

import org.joml.Vector2d;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Log;
import com.zephaniahnoah.lwjgl2D.Sound;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Player extends Ship implements Log {

	private static final Texture texture = new Texture("spaceinvaders/CoreDefender_A_209x182");
	public int rateOfFire = 10;
	public int fireLimitTimer = 0;
	boolean shoot = true;
	protected static final Sound shootSound = new Sound("shoot");

	public Player() {
		super(SpaceInvaders.instance.window.width / 2, 50, 10);
		damper = 1.4f;
		size.set(70);
		hitBoxSize.set(34, 74);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (position.x > SpaceInvaders.instance.window.width) {
			position.x = SpaceInvaders.instance.window.width;
		}

		if (position.x < 0) {
			position.x = 0;
		}

		fireLimitTimer++;
		if (fireLimitTimer > rateOfFire) {
			shoot = true;
			fireLimitTimer = 0;
		}
	}

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		super.draw(delta, lerpX, lerpY, game);

		SpaceInvaders g = (SpaceInvaders) game;

		if (Controls.left()) {
			velocity.x = -4;
		}

		if (Controls.right()) {
			velocity.x = 4;
		}

		if (shoot) {
			if (Controls.shoot()) {
				Bullet bullet = new Bullet(new Vector2d(position.x, position.y + 40), this);
				bullet.lastPosition.set(position.x, position.y + 33);
				shootSound.play();

				g.world.addEntity(bullet);
				shoot = false;
				fireLimitTimer = 0;
			}
		}

		drawHP(game.texturedShader, 63, 17, 100, 12);
	}

	@Override
	public Texture getTexture() {
		return texture;
	}
}
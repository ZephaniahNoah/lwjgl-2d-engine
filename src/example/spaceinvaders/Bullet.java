package example.spaceinvaders;

import java.awt.Color;

import org.joml.Vector2d;
import org.joml.Vector2f;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Bullet extends Entity {

	public static Texture texture;
	public float direction = 1;
	public Color color = Color.WHITE;
	public Ship shooter;

	public Bullet(Ship shooter) {
		this(new Vector2d(), shooter);
	}

	public Bullet(Vector2d position, Ship shooter) {
		super(position, new Vector2f(12, 24));
		solid = false;
		size.set(getTexture().width);
		this.shooter = shooter;
	}

	@Override
	public void update(float delta) {
		velocity.y = 10f * direction;

		if (position.y > SpaceInvaders.instance.window.height + size.y) {
			delete = true;
		}

		if (position.y < 0 - size.y) {
			delete = true;
		}
	}

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		game.texturedShader.loadColor(color);
		getTexture().bindAndDraw(game.texturedShader);
	}

	@Override
	public Texture getTexture() {
		return texture;
	}
}
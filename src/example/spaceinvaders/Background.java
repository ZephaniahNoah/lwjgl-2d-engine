package example.spaceinvaders;

import java.awt.Color;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Log;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Graphics;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Background extends Entity implements Log {

	private static Texture texture = new Texture(500, 700, Color.RED);
	private SpaceShader shader = new SpaceShader();

	public Background() {
		solid = false;
	}

	@Override
	public Texture getTexture() {
		return texture;
	}

	float time = 0;

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		game.texturedShader.stop();

		shader.start();

		time += delta / 30f;

		shader.loadTime(time);
		shader.loadProjectionMatrix(Game.projectionMatrix);
		shader.loadTransformationMatrix(Graphics.updateTransformationMatrix(this, position.x, position.y, rotation));
		getTexture().bindAndDraw(shader);

		shader.stop();

		game.texturedShader.start();
	}
}
package example.spaceinvaders;

import static org.lwjgl.glfw.GLFW.*;

import com.zephaniahnoah.lwjgl2D.Input;

public class Controls extends Input {

	public static boolean shoot() {
		return keys[GLFW_KEY_SPACE];
	}

	public static boolean left() {
		return keys[GLFW_KEY_A];
	}

	public static boolean right() {
		return keys[GLFW_KEY_D];
	}
}
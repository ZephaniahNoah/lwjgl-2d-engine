package example.spaceinvaders;

import com.zephaniahnoah.lwjgl2D.Log;

public class Wave implements Log {

	private static int timer = 0;

	public void tick(SpaceInvaders game) {
		timer++;

		if (timer > 100) {

			EnemyShip ship = new EnemyShip();

			log("Spawning enemy ship");

			ship.initPos(SpaceInvaders.rand.nextInt(game.window.width), game.window.height + ship.getTexture().height / 2);

			game.world.addEntity(ship);

			timer = 0;
		}
	}
}
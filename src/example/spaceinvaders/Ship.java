package example.spaceinvaders;

import java.awt.Color;

import com.zephaniahnoah.lwjgl2D.Sound;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Graphics;
import com.zephaniahnoah.lwjgl2D.gfx.shaders.TexturedShader;

public abstract class Ship extends Entity {

	private static final Color healthColor = new Color(101, 253, 49);
	protected static final Sound damage = new Sound("damage");
	protected static final Sound explosion = new Sound("explosion");
	public int maxHP;
	public int HP;

	public Ship(int x, int y, int hp) {
		super(x, y);
		maxHP = hp;
		HP = maxHP;
	}

	public Ship() {
		this(0, 0, 3);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		checkBullets();
	}

	public void checkBullets() {
		for (Entity ent : SpaceInvaders.instance.world.entities) {
			if (ent instanceof Bullet) {
				Bullet bullet = (Bullet) ent;
				if (!bullet.shooter.getClass().equals(this.getClass())) {
					if (bullet.collided(this) && !bullet.delete) {
						HP--;
						damage.play();
						bullet.delete = true;
					}
				}
			}
		}

		if (HP <= 0) {
			delete = true;
			explosion.play();
		}
	}

	public void drawHP(TexturedShader shader, double X, double Y, int width, int height) {
		float frac = (float) width / (float) maxHP;
		int missingHP = maxHP - HP;

		// Outline
		shader.loadColor(Color.BLACK);
		shader.loadTransformationMatrix(Graphics.updateTransformationMatrix((float) X, (float) Y, z, 0, 0, 0, width + 2, height + 2, matrix));
		SpaceInvaders.pixel.bindAndDraw(shader);

		// Background
		shader.loadColor(Color.RED);
		shader.loadTransformationMatrix(Graphics.updateTransformationMatrix((float) X, (float) Y, z, 0, 0, 0, width, height, matrix));
		SpaceInvaders.pixel.bindAndDraw(shader);

		// Remaining health
		shader.loadColor(healthColor);
		shader.loadTransformationMatrix(Graphics.updateTransformationMatrix((float) X - (frac * missingHP) / 2, (float) Y, z, 0, 0, 0, (HP * frac), height, matrix));
		SpaceInvaders.pixel.bindAndDraw(shader);
	}
}
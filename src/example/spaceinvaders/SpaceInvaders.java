package example.spaceinvaders;

import static org.lwjgl.glfw.GLFW.*;

import java.awt.Color;

import org.joml.Random;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;
import com.zephaniahnoah.lwjgl2D.gfx.gui.GuiScene;

public class SpaceInvaders extends Game {

	public static Texture pixel;
	public static final Random rand = new Random();
	public Background bg;
	public Player player;
	public static SpaceInvaders instance;
	public Wave wave = new Wave();
	public GuiScene<SpaceInvaders> world;

	public SpaceInvaders() {
		super("Space Invaders", 500, 700);
		instance = this;
		currentGui = world = new GuiWorld(this);
		world.addEntity(bg = new Background());
		world.addEntity(player = new Player());
		bg.position.set(window.width / 2, window.height / 2);
		bg.size.set(window.width, window.height);

		// Init texture from proper thread.
		EnemyShip.texture = new Texture("spaceinvaders/LPST_Evo_Magma");
		Bullet.texture = new Texture("spaceinvaders/bullet1");
		pixel = new Texture(1, 1, Color.GRAY);

		// addEntity(new EnemyShip(new Vector2d()));

	}

	@Override
	protected void update(float delta) {
		super.update(delta);
		wave.tick(this);
	}

	@Override
	public int targetTPS() {
		return 20;
	}

	@Override
	public int targetFPS() {
		return 60;
	}

	@Override
	public void windowInit() {
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	}
}
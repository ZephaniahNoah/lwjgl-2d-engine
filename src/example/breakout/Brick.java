package example.breakout;

import java.awt.Color;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Sound;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Brick extends Entity {

	public static final Texture texture = new Texture(64, 10, Color.WHITE);
	private Color color;
	private static final Sound breakSound = new Sound("break");

	public Brick(Color c, double x, double y) {
		super(x, y);
		color = c;
	}

	@Override
	public Texture getTexture() {
		return texture;
	}

	@Override
	public void update(float delta) {
		Breakout g = Breakout.instance;
		Ball ball = g.ball;

		if (collided(ball) && !delete) {
			delete = true;
			g.bricks--;
			breakSound.play();

			if (ball.lastPosition.x > position.x + size.x / 2) {
				// Right side
				ball.bounceDirection.set(ball.bounceDirection.x * -1, ball.bounceDirection.y, 0);
			}

			if (ball.lastPosition.x < position.x - size.x / 2) {
				// Left side
				ball.bounceDirection.set(ball.bounceDirection.x * -1, ball.bounceDirection.y, 0);
			}

			if (ball.lastPosition.y > position.y + size.y / 2) {
				// Top side
				ball.bounceDirection.set(ball.bounceDirection.x, ball.bounceDirection.y * -1, 0);
			}

			if (ball.lastPosition.y < position.y - size.y / 2) {
				// Bottom side
				ball.bounceDirection.set(ball.bounceDirection.x, ball.bounceDirection.y * -1, 0);
			}
		}
	}

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		game.texturedShader.loadColor(color);
		texture.bindAndDraw(game.texturedShader);
	}
}
package example.breakout;

import static org.lwjgl.glfw.GLFW.*;

import java.awt.Color;

import org.joml.Random;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.gfx.Camera;
import com.zephaniahnoah.lwjgl2D.gfx.Graphics;
import com.zephaniahnoah.lwjgl2D.gfx.gui.GuiScene;

public class Breakout extends Game {

	private final Paddle paddle;
	public final Ball ball;
	public static Breakout instance;
	public static final Random rand = new Random();
	private static final Color[] brickColors = new Color[] { //
			new Color(207, 33, 33), //
			new Color(235, 106, 18), //
			new Color(252, 209, 44), //
			new Color(21, 148, 37), //
			new Color(62, 145, 255) };
	public boolean gameOver = false;
	public int bricks;
	public GuiScene<Breakout> world;

	public Breakout() {
		super(Breakout.class.getSimpleName(), 800, 600);

		currentGui = world = new GuiWorld(this);

		instance = this;
		world.addEntity(paddle = new Paddle());
		world.addEntity(ball = new Ball(this));

		paddle.position.y = paddle.size.y / 2 + 32;
		paddle.position.x = window.width / 2;
		paddle.damper = 1.3f;

		// Create bricks
		float width = 11;
		float height = 5;
		bricks = (int) (width * height);
		for (float x = 0; x < width; x++) {
			for (float y = 0; y < height; y++) {

				float xPos = ((float) window.width / width) * x + ((float) window.width / width) / 2f;
				float yPos = (float) window.height - 20f * (y + 1f) - 15f;

				world.addEntity(new Brick(brickColors[(int) y], xPos, yPos));
			}
		}
	}

	@Override
	public void draw(float delta) {
		texturedShader.start();
		texturedShader.loadProjectionMatrix(projectionMatrix);
		
		currentGui.draw(delta);

		// Make camera follow he ball
		// Camera.fallowX = ball.position.x - window.width / 2;
		// Camera.fallowY = ball.position.y - window.height / 2;

		// Stop camera following when the game ends
		if (gameOver) {
			Camera.fallowX = 0;
			Camera.fallowY = 0;
		}

		// UI Stuff
		Graphics.enableBlending();
		Graphics.drawText(fps, 0, window.height, font, texturedShader, Color.GREEN); // Draw framerate

		String text = "Game Over";
		Color color = Color.RED;

		if (bricks == 0) {
			text = "You Win!";
			color = Color.GREEN;
			gameOver = true;
		}

		if (gameOver) {
			int width = font.width / 16;
			int half = (text.length() * width) / 2;
			Graphics.drawText(text, window.width / 2 - half, window.height / 2, font, texturedShader, color); // Draw Game Over text
		}

		Graphics.disableBlending();

		texturedShader.stop();
	}

	@Override
	public void windowInit() {
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	}

	@Override
	public int targetFPS() {
		return 60;
	}

	@Override
	public int targetTPS() {
		return 20;
	}
}

package example.breakout;

import java.awt.Color;

import org.joml.Math;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Paddle extends Entity {

	private static final Texture texture = new Texture(64, 10, new Color(13, 87, 186));
	private static final float speed = 5;

	@Override
	public Texture getTexture() {
		return texture;
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		Ball ball = Breakout.instance.ball;

		if (collided(ball)) {
			ball.position.set(ball.lastPosition);
			ball.bounceDirection.set(ball.bounceDirection.x, ball.bounceDirection.y * -1, 0);
			Breakout.instance.ball.bounceDirection.rotateZ(Math.toRadians(rotation * 2));
			ball.updateBouceDir();
			Ball.hitSound.play();
		}
	}

	@Override
	public void draw(float delta, double lerpX, double lerpY, Game game) {
		game.texturedShader.loadColor(Color.WHITE);
		texture.bindAndDraw(game.texturedShader);

//		Graphics.drawText(String.format("%.2f", position.x) + " " + String.format("%.2f", position.y), (int) Math.round(lerpX), (int) Math.round(lerpY), game.font, game.texturedShader, color);
//		// Must rebind the texture for the next entity which uses the same instance given by getTexture()
//		GL11.glBindTexture(GL11.GL_TEXTURE_2D, getTexture().textureID);

		if (((Breakout) game).gameOver)
			return;

		if (Controls.spinUp()) {
			rotation += delta;
			if (rotation >= 45) {
				rotation = 45;
			}
		}
		if (Controls.spinDown()) {
			rotation -= delta;
			if (rotation <= -45) {
				rotation = -45;
			}
		}

		if (Controls.left()) {
			if (position.x > size.x / 2f) {
				velocity.x = -delta * speed;
			}
		} else if (Controls.right()) {
			if (position.x < game.window.width - size.x / 2f) {
				velocity.x = delta * speed;
			}
		}

		if (position.x < size.x / 2f) {
			position.x = size.x / 2f;
			velocity.x = 0;
		}

		if (position.x > game.window.width - size.x / 2f) {
			position.x = game.window.width - size.x / 2f;
			velocity.x = 0;
		}
	}
}
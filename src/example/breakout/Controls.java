package example.breakout;

import static org.lwjgl.glfw.GLFW.*;

import com.zephaniahnoah.lwjgl2D.Input;

public class Controls extends Input {

	public static boolean spinUp() {
		return keys[GLFW_KEY_LEFT];
	}

	public static boolean spinDown() {
		return keys[GLFW_KEY_RIGHT];
	}

	public static boolean left() {
		return keys[GLFW_KEY_A];
	}

	public static boolean right() {
		return keys[GLFW_KEY_D];
	}
}
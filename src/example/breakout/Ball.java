package example.breakout;

import org.joml.Vector2d;
import org.joml.Vector2f;
import org.joml.Vector3f;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Sound;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public class Ball extends Entity {

	private static final Texture texture = new Texture("ball");
	public float speed = 8;// 7;
	public Vector3f bounceDirection = new Vector3f(Breakout.rand.nextInt(Math.round(10)) - 5, -speed, 0);
	protected static final Sound hitSound = new Sound("hit");
	protected static final Sound gameOverSound = new Sound("game_over");

	public Ball(Game game) {
		super(new Vector2d(game.window.width / 2, game.window.height / 2), new Vector2f(10, 10));
		solid = false;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (position.x < 0) {
			// Left
			bounceDirection.set(bounceDirection.x * -1, bounceDirection.y, 0);
			position.x = 0;
			hitSound.play();
		}

		if (position.x > Breakout.instance.window.width) {
			// Right
			bounceDirection.set(bounceDirection.x * -1, bounceDirection.y, 0);
			position.x = Breakout.instance.window.width;
			hitSound.play();
		}

		if (position.y < 0) {
			// Bottom
			Breakout.instance.gameOver = true;
			this.delete = true;
			if (!gameOverSound.isPlaying()) {
				gameOverSound.play();
			}
		}

		if (position.y > Breakout.instance.window.height) {
			// Top
			bounceDirection.set(bounceDirection.x, bounceDirection.y * -1, 0);
			position.y = Breakout.instance.window.height;
			hitSound.play();
		}

		updateBouceDir();
	}

	public void updateBouceDir() {
		Vector3f vec = bounceDirection.normalize().mul(speed);
		velocity.set(vec.x, vec.y);
	}

	@Override
	public Texture getTexture() {
		return texture;
	}
}
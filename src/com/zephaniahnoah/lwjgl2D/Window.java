package com.zephaniahnoah.lwjgl2D;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL43.*;

//import org.joml.Math;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLDebugMessageCallback;
import org.lwjgl.system.MemoryUtil;

import com.zephaniahnoah.lwjgl2D.gfx.Framebuffer;

public class Window implements Log {

	public int width;
	public int height;
	public long id;
	private String name;
	private Game game;
	private double lastFrame = 0;
	private double lastTick = 0;
	private boolean open = true;
	protected GLCapabilities glCapabilities;
	private long audioContext;
	private long audioDevice;

	public Window(String name, int width, int height, Game game) {
		this.name = name;
		this.width = width;
		this.height = height;
		this.game = game;
		log("JVM: " + System.getProperty("java.version"), name);
		log("LWJGL: " + Version.getVersion(), name);
		init();
		Input.init(id, game);

		// Set the swap interval to prevent lag caused by glfwSwapBuffers.
		glfwSwapInterval(0);
	}

	public void run() {
		loop();
		end();
	}

	private void init() {

		glfwSetErrorCallback(new GLFWErrorCallback() {
			@Override
			public void invoke(int error, long description) {
				log(getDescription(description), "GLFW");
			}
		});

		// Try to initialize GLFW.
		if (!glfwInit()) {
			log("Failed to initialize GLFW!");
			return;
		}

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

		game.windowInit();

		id = glfwCreateWindow((int) width, (int) height, name, 0, 0);
		glfwMakeContextCurrent(id);

		// vsync
		glfwSwapInterval(1);

		glfwShowWindow(id);

		// Init audio device
		String defaultDevideName = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);
		audioDevice = alcOpenDevice(defaultDevideName);

		int[] attributes = { 0 };
		audioContext = alcCreateContext(audioDevice, attributes);
		alcMakeContextCurrent(audioContext);

		ALCCapabilities alcCapabilities = ALC.createCapabilities(audioDevice);
		ALCapabilities alCapabilities = AL.createCapabilities(alcCapabilities);

		if (!alCapabilities.OpenAL10) {
			log("Audio library not supported.");
		}

		glCapabilities = GL.createCapabilities();

		glfwSetWindowSizeCallback(id, new GLFWWindowSizeCallback() {
			@Override
			public void invoke(long window, int argWidth, int argHeight) {
				glViewport(0, 0, argWidth, argHeight);
				width = argWidth;
				height = argHeight;
				if (game.useFrameBuffer) {
					if (game.framebuffer != null)
						game.framebuffer.unbind();
					if (game.pixelPerfectMode) {
						game.framebuffer = new Framebuffer((int) scaledWidth(), (int) scaledHeight());
					} else {
						game.framebuffer = new Framebuffer(width, height);
					}
				}
				game.resetProjectionMatrix();
			}
		});

		if (this.glCapabilities.OpenGL43) {
			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(new GLDebugMessageCallback() {
				@Override
				public void invoke(int source, int type, int id, int severity, int length, long message, long userParam) {
					String error = "";

					switch (severity) {
					case GL_DEBUG_SEVERITY_HIGH:
						error += "High";
						break;
					case GL_DEBUG_SEVERITY_MEDIUM:
						error += "Medium";
						break;
					case GL_DEBUG_SEVERITY_LOW:
						error += "Low";
						break;
					case GL_DEBUG_SEVERITY_NOTIFICATION:
						error += "Notification";
						break;
					}

					error += " level ";

					switch (type) {
					case GL_DEBUG_TYPE_ERROR:
						error += "error";
						break;
					case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
						error += "deprecated behavior";
						break;
					case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
						error += "undefined behavior";
						break;
					case GL_DEBUG_TYPE_PORTABILITY:
						error += "portability issue";
						break;
					case GL_DEBUG_TYPE_PERFORMANCE:
						error += "performance issue";
						break;
					case GL_DEBUG_TYPE_OTHER:
						error += "issue";
						break;
					case GL_DEBUG_TYPE_MARKER:
						error += "marker";
						break;
					}

					log(error + ": " + MemoryUtil.memUTF8(message), "OpenGL");
				}
			}, 0);
		}

		log("OpenGL: " + glGetString(GL_VERSION));
		log("GPU: " + glGetString(GL_RENDERER));
		log("GLFW: " + glfwGetVersionString());
	}

	protected void loop() {
		Thread gameLoop = new Thread("update") {
			@Override
			public void run() {
				try {
					while (open()) {

						double beforTickTime = Game.getMilliseconds();
						double interval = 1000.0D / game.targetTPS();

						if (beforTickTime >= lastTick + interval) {

							game.update((float) ((beforTickTime - lastTick) / interval));

							lastTick = beforTickTime;

							double tickTime = Game.getMilliseconds() - beforTickTime;

							Thread.sleep(Math.max(0, Math.round((interval - tickTime) - .4)));
						}
					}
				} catch (Exception e) {
					StaticLog.exception(e);
				}
			}
		};

		gameLoop.start();

		try {
			while (open()) {
				double beforeRenderTime = Game.getMilliseconds();
				double interval = 1000D / game.targetFPS();

				if (beforeRenderTime >= lastFrame + interval) {
					double d = (beforeRenderTime - lastFrame);

					game.fps = (int) Math.round(1000.0D / d);

					game.render((float) (d / interval));

					lastFrame = beforeRenderTime;

					double renderTime = Game.getMilliseconds() - beforeRenderTime;

					// log("took " + renderTime + " to render");

					Thread.sleep(Math.max(0, Math.round((interval - renderTime) - .4)));

					open = !glfwWindowShouldClose(id);
				}
			}
		} catch (Exception e) {
			StaticLog.exception(e);
		}
	}

	public float scaledWidth() {
		return width / game.getScale();
	}

	public float scaledHeight() {
		return height / game.getScale();
	}

	public boolean open() {
		return open;
	}

	private void end() {
		log("Closing.");
		Input.freeCallbacks();
		game.cleanUp();
		glfwDestroyWindow(id);
		glfwTerminate();
		alcDestroyContext(audioContext);
		alcCloseDevice(audioDevice);
	}
}
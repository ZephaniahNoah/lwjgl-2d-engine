package com.zephaniahnoah.lwjgl2D;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.awt.Color;

import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFW;

import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Framebuffer;
import com.zephaniahnoah.lwjgl2D.gfx.Graphics;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;
import com.zephaniahnoah.lwjgl2D.gfx.gui.Gui;
import com.zephaniahnoah.lwjgl2D.gfx.gui.GuiScene;
import com.zephaniahnoah.lwjgl2D.gfx.shaders.TexturedShader;

public abstract class Game implements Log {

	public final double frameTime;
	public final double tickTime;
	public TexturedShader texturedShader;
	public static Matrix4f projectionMatrix = new Matrix4f();
	public Window window;
	public int fps = 0;
	public Texture font;
	public float framesSinceLastTick = 0;
	private float scale = 1f;
	public boolean pixelPerfectMode = false;
	public boolean useFrameBuffer = false;
	public boolean frameBufferActive = false;
	public Framebuffer framebuffer;
	protected Timer logTimer = new Timer(10);
	public boolean scaleChanged;
	public Gui<?> currentGui;

	public Game(String name, int width, int height) {
		frameTime = 1000.0D / targetFPS();
		tickTime = 1000.0D / targetTPS();
		window = new Window(name, width, height, this);
		texturedShader = new TexturedShader();
		font = new Texture("font/taffer");

		Texture.initBufferObjects();

		resetProjectionMatrix();

		currentGui = new GuiScene<Game>(this) {
		};

		log("Initialized.");
	}

	public void run() {
		window.run();
	}

	public void render(float delta) throws Exception {
		frameBufferActive = useFrameBuffer;
		glfwPollEvents();
		if (scaleChanged) {
			resetProjectionMatrix();
			scaleChanged = false;
		}
		glClear(GL_COLOR_BUFFER_BIT);
		if (frameBufferActive)
			framebuffer.bind();
		draw(delta);
		if (frameBufferActive)
			framebuffer.unbind();
		drawBuffer(texturedShader);
		frameBufferActive = false;

		Input.pollInput();
		glfwSwapBuffers(window.id);
		framesSinceLastTick++;
	}

	protected void drawBuffer(TexturedShader shader) {
		if (useFrameBuffer) {
			shader.start();
			shader.loadColor(Color.WHITE);
			shader.loadProjectionMatrix(projectionMatrix);

			glActiveTexture(GL_TEXTURE0);

			if (pixelPerfectMode) {
				shader.loadTransformationMatrix(Graphics.updateTransformationMatrix(window.width / 2f, window.height / 2f, 0, 0, 180, 180, window.width, window.height, Entity.matrix));
			} else {
				shader.loadTransformationMatrix(Graphics.updateTransformationMatrix(window.scaledWidth() / 2f, window.scaledHeight() / 2f, 0, 0, 180, 180, window.scaledWidth(), window.scaledHeight(), Entity.matrix));
			}

			framebuffer.texture.bindAndDraw(shader);

			shader.stop();
		}
	}

	protected void draw(float delta) {
		texturedShader.start();

		// glClearColor(.2f, .45f, .2f, 1);
		// if (useFrameBuffer())
		// glClear(GL_COLOR_BUFFER_BIT);

		texturedShader.loadProjectionMatrix(projectionMatrix);

		currentGui.draw(delta);

		texturedShader.stop();
	}

	public static double getMilliseconds() {
		return GLFW.glfwGetTime() * 1000.0D;
	}

	public void cleanUp() {
		texturedShader.delete();
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		scaleChanged = true;
		this.scale = scale;
	}

	public void resetProjectionMatrix() {
		projectionMatrix.identity();
		if (pixelPerfectMode && useFrameBuffer) {
			projectionMatrix.ortho2D(0, window.width, 0, window.height);
			framebuffer = new Framebuffer((int) window.scaledWidth(), (int) window.scaledHeight());
		} else {
			projectionMatrix.ortho2D(0, window.width / scale, 0, window.height / scale);
			if (useFrameBuffer)
				framebuffer = new Framebuffer(window.width, window.height);
		}

		// Update the mouse position
		Input.mouseXScaled = Input.mouseX / scale;
		Input.mouseYScaled = Input.mouseY / scale;
	}

	protected void update(float delta) {
		framesSinceLastTick = 0;

		currentGui.update(delta);

		if (logTimer.time()) {
			StaticLog.write();
		}
	}

	public abstract int targetTPS();

	public abstract int targetFPS();

	public void windowInit() {
	}
}
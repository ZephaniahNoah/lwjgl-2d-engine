package com.zephaniahnoah.lwjgl2D;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

public class Input {

	public static final int keyCount = GLFW_KEY_LAST + 1;
	public static boolean[] keys = new boolean[keyCount];
	private static boolean[] hold = new boolean[keyCount];
	public static boolean[] singlePress = new boolean[keyCount];
	private static GLFWScrollCallback scrollCallback;
	private static GLFWKeyCallback keyCallback;
	private static GLFWCursorPosCallback cursorPosCallback;
	private static GLFWMouseButtonCallback mouseButtonCallback;
	public static double mouseXScaled = 0;
	public static double mouseYScaled = 0;
	public static double mouseY;
	public static double mouseX;

	public static boolean scrollUp;
	public static boolean scrollDown;

	public static boolean leftClickHold;
	public static boolean rightClickHold;
	public static boolean middleClickHold;
	public static boolean leftClickPress;
	public static boolean rightClickPress;
	public static boolean middleClickPress;
	public static boolean leftClickRelease;
	public static boolean rightClickRelease;
	public static boolean middleClickRelease;

	private static boolean leftSwitch = true;
	private static boolean rightSwitch = true;
	public static boolean middleSwitch = true;

	public static void init(long window, Game game) {
		glfwSetScrollCallback(window, scrollCallback = new GLFWScrollCallback() {
			@Override
			public void invoke(long window, double xoffset, double yoffset) {
				if (yoffset == 1) {
					scrollUp = true;
				} else if (yoffset == -1) {
					scrollDown = true;
				}
			}
		});

		glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			@Override
			public void invoke(long win, int key, int scancode, int action, int mods) {
				Input.event(win, key, scancode, action, mods);
			}
		});

		glfwSetCursorPosCallback(window, cursorPosCallback = new GLFWCursorPosCallback() {
			@Override
			public void invoke(long window, double xpos, double ypos) {
				mouseX = xpos;
				mouseY = ((ypos - game.window.height) * -1);
				mouseXScaled = mouseX / game.getScale();
				mouseYScaled = mouseY / game.getScale();
			}
		});

		glfwSetMouseButtonCallback(window, mouseButtonCallback = new GLFWMouseButtonCallback() {
			@Override
			public void invoke(long window, int button, int action, int mods) {
				if (button == GLFW_MOUSE_BUTTON_1) {
					leftClickHold = action == GLFW_PRESS;

					if (leftSwitch && action == GLFW_PRESS) {
						leftClickPress = true;
						leftSwitch = false;
					}

					if (!leftSwitch && action == GLFW_RELEASE) {
						leftClickRelease = true;
						leftSwitch = true;
					}
				} else if (button == GLFW_MOUSE_BUTTON_2) {
					rightClickHold = action == GLFW_PRESS;

					if (rightSwitch && action == GLFW_PRESS) {
						rightClickPress = true;
						rightSwitch = false;
					}

					if (!rightSwitch && action == GLFW_RELEASE) {
						rightClickRelease = true;
						rightSwitch = true;
					}
				} else if (button == GLFW_MOUSE_BUTTON_3) {
					middleClickHold = action == GLFW_PRESS;

					if (middleSwitch && action == GLFW_PRESS) {
						middleClickPress = true;
						middleSwitch = false;
					}

					if (!middleSwitch && action == GLFW_RELEASE) {
						middleClickRelease = true;
						middleSwitch = true;
					}
				}
			}
		});
	}

	private static void event(long window, int key, int scancode, int action, int mods) {
		if (key <= keyCount && key >= 0)
			keys[key] = (action == GLFW_PRESS || action == GLFW_REPEAT);// Index -1 out of bounds for length 349
	}

	public static void pollInput() {
		for (int i = 0; i < keyCount; i++) {
			singlePress[i] = false;// Reset all single press keys
			if (keys[i]) {// If key is pressed
				if (!hold[i]) {// But not being held down
					singlePress[i] = true;// Then it's a single press
				}
			}
			hold[i] = keys[i];// Store the held keys
		}

		// Reset click buttons
		leftClickPress = false;
		leftClickRelease = false;
		middleClickPress = false;
		rightClickPress = false;
		rightClickRelease = false;
		middleClickRelease = false;
		// Reset scroll buttons
		scrollUp = false;
		scrollDown = false;
	}

	public static void freeCallbacks() {
		keyCallback.free();
		cursorPosCallback.free();
		mouseButtonCallback.free();
		scrollCallback.free();
	}
}
package com.zephaniahnoah.lwjgl2D.entity;

import org.joml.Math;
import org.joml.Matrix4f;
import org.joml.Vector2d;
import org.joml.Vector2f;
import org.joml.Vector3f;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.gfx.Texture;

public abstract class Entity {

	public Vector2d position = new Vector2d();
	public Vector2d lastPosition = new Vector2d();
	public float z = 0;
	public float rotation;
	public float lastRotation;
	public Vector2f size;
	public Vector2f velocity = new Vector2f();
	public float damper = 1.05f;
	public static final Matrix4f matrix = new Matrix4f();
	public boolean delete = false;
	public boolean solid = true;
	public boolean pushable = false;

	// Hitbox stuff
	public Vector2f hitBoxSize = new Vector2f();
	public Vector2f hitBoxOffset = new Vector2f();

	// These coords aren't in world space. They are relative to this entity
	public Vector3f[] hitBox = { new Vector3f(), new Vector3f(), new Vector3f(), new Vector3f() };
	public Vector3f[] hitBoxAligned = { new Vector3f(), new Vector3f(), new Vector3f(), new Vector3f() };

	public Entity() {
		this(0, 0);
	}

	public Entity(double x, double y) {
		this(new Vector2d(x, y));
	}

	public Entity(double x, double y, double z, float width, float height) {
		this(new Vector2d(x, y), new Vector2f(width, height));
	}

	public Entity(Vector2d pos) {
		initPos(pos);
		size = new Vector2f(getTexture().width, getTexture().height);

		initHitbox();
	}

	public Entity(Vector2d pos, Vector2f scale) {
		initPos(pos);
		size = scale;

		initHitbox();
	}

	public void initPos(Vector2d pos) {
		position.set(pos);
		lastPosition.set(pos);
	}

	public void initPos(double x, double y) {
		position.set(x, y);
		lastPosition.set(position);
	}

	public void initHitbox() {
		hitBoxSize.set(0, 0);
		hitBoxOffset.set(0, 0);
		hitBoxSize.set(size.x, size.y);

		for (int i = 0; i < 4; i++) {
			hitBox[i].set(0, 0, 0);
			hitBoxAligned[i].set(0, 0, 0);
		}

		updateHitboxRotation();
	}

	public void updateHitboxRotation() {
		// hitBoxSize.set(size.x, size.y);

		hitBox[0].set(-(hitBoxSize.x / 2f), -(hitBoxSize.y / 2f), 0);// Bottom left
		hitBox[1].set(+(hitBoxSize.x / 2f), -(hitBoxSize.y / 2f), 0);// Bottom right
		hitBox[2].set(-(hitBoxSize.x / 2f), +(hitBoxSize.y / 2f), 0);// Top left
		hitBox[3].set(+(hitBoxSize.x / 2f), +(hitBoxSize.y / 2f), 0);// Top right

		for (int i = 0; i < 4; i++) {
			hitBox[i].rotateAxis(Math.toRadians(rotation), 0, 0, 1);
		}
	}

	public void onCollide(Entity e) {
	}

	public boolean collided(Entity e) {
		return this.isInside(e) || e.isInside(this);
	}

	// How to do AABB collision detection on rotated rectangles. Hehe..
	public boolean isInside(Entity e) {

		// Get coordinates of second entity relative to this entity (difference)
		float x = (float) (e.position.x - position.x);
		float y = (float) (e.position.y - position.y);

		for (int i = 0; i < 4; i++) {
			// Make a copy of the corner coordinates and add the difference between the two entities
			hitBoxAligned[i].set(e.hitBox[i].x + x, e.hitBox[i].y + y, 0);
			// Rotate the copy so they are now axis aligned
			hitBoxAligned[i].rotateAxis(Math.toRadians(-rotation), 0, 0, 1);
		}

		// Do simple axis aligned collision detection
		return pointWithinRelative(hitBoxAligned[0]) || pointWithinRelative(hitBoxAligned[1]) || pointWithinRelative(hitBoxAligned[2]) || pointWithinRelative(hitBoxAligned[3]);
	}

	public boolean pointWithinRelative(Vector3f p) {
		return p.x < (hitBoxSize.x / 2 + hitBoxOffset.x) && p.x > -(hitBoxSize.x / 2 + hitBoxOffset.x) && p.y < (hitBoxSize.y / 2 + hitBoxOffset.y) && p.y > -(hitBoxSize.y / 2 + hitBoxOffset.y);
	}

	public void rotate(float degrees) {
		rotation += degrees;
	}

	public abstract Texture getTexture();

	public void update(float delta) {
		updateHitboxRotation();
	}

	public void draw(float delta, double lerpX, double lerpY, Game game) {
		game.texturedShader.loadColor(1f, 1f, 1f);
		getTexture().bindAndDraw(game.texturedShader);
	}

	public boolean isMoving() {
		return !(lastPosition.equals(position) && rotation == lastRotation && velocity.equals(.0f, .0f));
	}
}
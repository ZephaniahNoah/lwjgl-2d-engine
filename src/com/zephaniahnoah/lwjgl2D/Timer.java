package com.zephaniahnoah.lwjgl2D;

public class Timer {

	public int time = 0;
	public int timeLimit;

	public Timer(int limit) {
		this.timeLimit = limit;
	}

	public boolean time() {
		time++;
		if (time > timeLimit) {
			time = 0;
			return true;
		}
		return false;
	}

	public int remaining() {
		return timeLimit - time;
	}

	public void reset() {
		time = 0;
	}

	public void end() {
		time = timeLimit;
	}
}

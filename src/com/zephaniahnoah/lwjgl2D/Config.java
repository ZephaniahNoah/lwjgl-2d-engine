package com.zephaniahnoah.lwjgl2D;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Config {

	private static final String configDirectory = "config/";
	private String configName;
	private final Map<String, String> values = new LinkedHashMap<String, String>();

	public Config(String name) {
		configName = name + ".ini";
	}

	public String getString(String name, String defaultValue) {
		if (values.containsKey(name))
			return values.get(name);
		values.put(name, defaultValue);
		return defaultValue;
	}

	public double getDouble(String name, Double defaultValue) {
		if (values.containsKey(name))
			return Double.parseDouble(values.get(name));
		values.put(name, defaultValue.toString());
		return defaultValue;
	}

	public float getFloat(String name, Float defaultValue) {
		if (values.containsKey(name))
			return Float.parseFloat(values.get(name));
		values.put(name, defaultValue.toString());
		return defaultValue;
	}

	public int getInt(String name, Integer defaultValue) {
		if (values.containsKey(name))
			return Integer.parseInt(values.get(name));
		values.put(name, defaultValue.toString());
		return defaultValue;
	}

	public boolean getBoolean(String name, Boolean defaultValue) {
		if (values.containsKey(name))
			return Boolean.parseBoolean(values.get(name).toLowerCase());
		values.put(name, defaultValue.toString());
		return defaultValue;
	}

	public void set(String name, Object value) {
		values.put(name, value.toString());
	}

	public void saveConfig() {
		checkFolder();
		FileWriter writer;
		try {
			writer = new FileWriter(new File(configDirectory + configName));
			for (Entry<String, String> pair : values.entrySet()) {
				if (pair.getValue() != null) {
					writer.write(pair.getKey() + "=" + pair.getValue() + "\n");
				}
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void checkFolder() {
		File folder = new File(configDirectory);
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	public void load() {
		checkFolder();
		File file = new File(configDirectory + configName);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (!line.startsWith("#") && line.contains("=")) {
					String[] split = line.split("=");
					if (split[1].startsWith(" ")) {
						split[1].substring(1);
					}
					values.put(split[0].replaceAll(" ", ""), split[1]);
				}
			}
			reader.close();
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

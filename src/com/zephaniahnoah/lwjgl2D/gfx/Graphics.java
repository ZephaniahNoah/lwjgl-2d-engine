package com.zephaniahnoah.lwjgl2D.gfx;

import static org.lwjgl.opengl.GL14.*;

import java.awt.Color;

import org.joml.Matrix4f;

import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.shaders.TexturedShader;

public class Graphics {

	private static final Matrix4f fontMatrix = new Matrix4f();

//	public static Matrix4f updateTransformationMatrix(Entity ent) {
//		return updateTransformationMatrix(ent, ent.position.x, ent.position.y, ent.rotation);
//	}

	public static Matrix4f updateTransformationMatrix(Entity ent, double x, double y, float rotation) {
		return updateTransformationMatrix(x - Camera.positionI.x, y - Camera.positionI.y, ent.z, rotation, ent.size.x, ent.size.y, Entity.matrix);
	}

	public static Matrix4f updateTransformationMatrix(double x, double y, double z, float rotation, float width, float height, Matrix4f matrix) {
		return updateTransformationMatrix((float) x, (float) y, (float) z, 0, 0, rotation, width, height, matrix);
	}

	public static Matrix4f updateTransformationMatrix(float x, float y, float z, Matrix4f matrix) {
		return updateTransformationMatrix(x, y, z, 0, 0, 0, 1, 1, matrix);
	}

	public static Matrix4f updateTransformationMatrix(float x, float y, float z, float rx, float ry, float rz, float width, float height, Matrix4f matrix) {
		matrix.identity();

		matrix.translate(x, y, z, matrix);

		matrix.rotate((float) Math.toRadians(rx), 1, 0, 0, matrix);
		matrix.rotate((float) Math.toRadians(ry), 0, 1, 0, matrix);
		matrix.rotate((float) Math.toRadians(rz), 0, 0, 1, matrix);

		matrix.scale(width, height, 1, matrix);
		return matrix;
	}

	public static void drawText(Object text, int x, int y, Texture font, TexturedShader shader) {
		drawText(text, x, y, font, shader, Color.WHITE);
	}

	public static void drawText(Object text, int x, int y, Texture font, TexturedShader shader, Color c) {
		drawText(text, x, y, font, shader, c, 0);
	}

	public static void drawText(Object text, int x, int y, Texture font, TexturedShader shader, Color c, int padding) {
		char[] letters = text.toString().toCharArray();

		int width = font.width / 16;
		int height = font.height / 16;

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, font.textureID);
		Graphics.enableBlending();

		for (int i = 0; i < letters.length; i++) {
			int letter = letters[i];
			shader.loadTransformationMatrix(updateTransformationMatrix(x + width / 2 + i * width + i * padding, y - height / 2, 0, 0, width, height, fontMatrix));

			int X = (letter % 16) * width;
			int Y = (letter / 16) * height;

			shader.loadColor(c);
			font.draw(shader, X, Y, X + width, Y + height);
		}

		Graphics.disableBlending();
	}

	public static void drawTextWrapped(Object text, int x, int y, Texture font, TexturedShader shader, Color c, int limit, int padding) {
		String[] words = text.toString().split(" ");
		String line = "";
		int charSize = font.width / 16;
		int lines = 0;

		for (String word : words) {

			if (line.length() + word.length() < limit) {
				line += word + " ";
			} else {
				drawText(line, x, y - (lines * charSize), font, shader, c);
				line = word + " ";
				lines++;
			}
		}
		drawText(line, x, y - (lines * charSize), font, shader, c, padding);
	}

	public static void enableBlending() {
		glEnable(GL_BLEND);
		glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);
	}

	public static void disableBlending() {
		glDisable(GL_BLEND);
	}
}
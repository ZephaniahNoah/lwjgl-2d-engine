package com.zephaniahnoah.lwjgl2D.gfx.gui;

import static org.lwjgl.opengl.GL13.*;

import java.util.ArrayList;
import java.util.List;

import org.joml.Math;

import com.zephaniahnoah.lwjgl2D.Game;
import com.zephaniahnoah.lwjgl2D.Util;
import com.zephaniahnoah.lwjgl2D.entity.Entity;
import com.zephaniahnoah.lwjgl2D.gfx.Camera;
import com.zephaniahnoah.lwjgl2D.gfx.Graphics;

public abstract class GuiScene<G extends Game> extends Gui<G> {

	public List<Entity> entities = new ArrayList<Entity>();
	private List<Entity> entitiesToAdd = new ArrayList<Entity>();
	private ArrayList<Entity> addedEntities = new ArrayList<Entity>();

	public GuiScene(G g) {
		super(g);
	}

	public void addEntity(Entity ent) {
		if (ent == null)
			return;
		for (Object o : addedEntities.toArray()) {
			Entity e = (Entity) o;
			entitiesToAdd.remove(e);
		}
		entitiesToAdd.add(ent);
	}

	public void filterEntities() {
		List<Entity> toRemove = new ArrayList<Entity>();
		for (Entity ent : entities)
			if (ent.delete)
				toRemove.add(ent);
		entities.removeAll(toRemove);

		for (Object o : entitiesToAdd.toArray()) {
			Entity ent = (Entity) o;
			if (entities.contains(ent))
				continue;
			boolean added = false;
			for (int i = 0; i < entities.size(); i++) {
				float z = entities.get(i).z;
				if (z > ent.z) {
					entities.add(i, ent);
					entitiesToAdd.remove(ent);
					added = true;
					break;
				}
			}
			if (!added)
				entities.add(ent);
			addedEntities.add(ent);
			entitiesToAdd.remove(ent);
		}
	}

	@Override
	public void draw(G g, float delta) {
		// Draw framerate
		Graphics.drawText(g.fps, 0, g.window.height, g.font, g.texturedShader);
		drawEntities(g, delta);
		filterEntities();
	}

	public void drawEntities(G g, float delta) {
		float interpolateFraction = g.fps / g.targetTPS();
		float position = g.framesSinceLastTick / interpolateFraction;

		if (g.pixelPerfectMode)
			position = 1;

		Camera.update(delta);

		glActiveTexture(GL_TEXTURE0);

		for (int i = 0; i < entities.size(); i++) {
			Entity ent = entities.get(i);

			if (!ent.delete) {
				double x = Math.lerp(ent.lastPosition.x, ent.position.x, position);
				double y = Math.lerp(ent.lastPosition.y, ent.position.y, position);

				float rotation = Math.lerp(ent.lastRotation, ent.rotation, position);

				g.texturedShader.loadTransformationMatrix(Graphics.updateTransformationMatrix(ent, x, y, rotation));

				ent.draw(delta, x, y, g);
			}
		}
	}

	@Override
	public void update(float delta) {
		Object[] entityList = entities.toArray();
		for (int i = entityList.length - 1; i > -1; i--) {
			Entity ent = (Entity) entityList[i];
			if (!ent.delete) {
				ent.lastRotation = ent.rotation;
				ent.lastPosition.set(ent.position);

				ent.position.add(ent.velocity.x * delta, ent.velocity.y * delta);

				ent.velocity.div(ent.damper);

				if (Util.roughEquals(ent.lastPosition, ent.position)) {
					ent.velocity.set(0);
				}

				// Stops 1 pixel before it actually touches. Not sure why :(
				Object[] array = entities.toArray();
				if (ent.solid) {
					if (ent.isMoving()) {
						for (int k = 0; k < array.length; k++) {
							try {
								Entity entTouching = (Entity) array[k];
								if (entTouching != null && entTouching != ent && entTouching.solid && entTouching.collided(ent)) {

									ent.onCollide(entTouching);
									entTouching.onCollide(ent);

									ent.position.set(ent.lastPosition);

									float absX = Math.abs(ent.velocity.x);
									float absY = Math.abs(ent.velocity.y);

									float highest = (absX > absY ? absX : absY) * 2f;

									if (highest == 0) {
										highest = 1;
									}

									float smallX = (ent.velocity.x / highest) / 2f;
									float smallY = (ent.velocity.y / highest) / 2f;

									double x = ent.position.x - entTouching.position.x;
									double y = ent.position.y - entTouching.position.y;

									for (int j = 0; j < highest + 1; j++) {
										ent.position.add(smallX, smallY);
										if (entTouching.collided(ent)) {
											ent.position.set(ent.lastPosition);
											break;
										}
										ent.lastPosition.set(ent.position);
									}

									if (Math.abs(x) > Math.abs(y)) {
										ent.velocity.x = 0;
									} else {
										ent.velocity.y = 0;
									}
								}
							} catch (IndexOutOfBoundsException ex) {
								ex.printStackTrace();
								break;
							}
						}
					}

					if (ent.pushable) {
						for (int k = 0; k < array.length; k++) {
							try {
								Entity entTouching = (Entity) array[k];
								if (entTouching.solid && entTouching.collided(ent)) {
									double x = ent.position.x - entTouching.position.x;
									double y = ent.position.y - entTouching.position.y;

									double absX = Math.abs(x);
									double absY = Math.abs(y);

									double highest = (absX > absY ? absX : absY);

									float smallX = (float) (x / highest);
									float smallY = (float) (y / highest);

									ent.position.add(smallX * 2, smallY * 2);
								}

							} catch (IndexOutOfBoundsException ex) {
								ex.printStackTrace();
								break;
							}
						}
					}
				}

				ent.update(delta);
			}
		}
	}
}
package com.zephaniahnoah.lwjgl2D.gfx.gui;

import com.zephaniahnoah.lwjgl2D.Game;

public abstract class Gui<G extends Game> {

	private G game;

	public Gui(G instance) {
		game = instance;
	}

	public void draw(float delta) {
		this.draw(game, delta);
	}

	public abstract void draw(G g, float delta);

	public void update(float delta) {
		this.update(game, delta);
	}

	public void update(G g, float delta) {

	}

	public void onWindowResize(G g) {

	}
}
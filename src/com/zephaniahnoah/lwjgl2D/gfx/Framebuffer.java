package com.zephaniahnoah.lwjgl2D.gfx;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.ARBFramebufferObject.*;

import java.awt.Color;

import com.zephaniahnoah.lwjgl2D.Log;

public class Framebuffer implements Log {

	public int fboId;
	public Texture texture;

	public Framebuffer(int width, int height) {
		fboId = glGenFramebuffers();
		bind();

		texture = new Texture(width, height, Color.BLACK);
		texture.hasAlpha = true;
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.textureID, 0);

		int rboId = glGenRenderbuffers();
		glBindRenderbuffer(GL_RENDERBUFFER, rboId);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboId);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			log("ERROR! Framebuffer is not complete!");
		}

		unbind();
	}

	public void bind() {
		glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	}

	public void unbind() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

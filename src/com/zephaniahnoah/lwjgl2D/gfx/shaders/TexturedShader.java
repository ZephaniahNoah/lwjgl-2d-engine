package com.zephaniahnoah.lwjgl2D.gfx.shaders;

import static org.lwjgl.opengl.GL20.*;

import java.awt.Color;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class TexturedShader extends Shader {

	private int location_transformationMatrix, //
			/**/location_projectionMatrix, //
			/**/location_textureOffset, //
			/**/location_color;

	public TexturedShader() {
		super("textured");
	}

	public TexturedShader(String name) {
		super(name);
	}

	public void loadColor(Vector3f c) {
		loadColor(c.x, c.y, c.z, 1.0f);
	}

	public void loadColor(Color c) {
		loadColor(c.getRed() / 256f, c.getGreen() / 256f, c.getBlue() / 256f, 1.0f);
	}

	public void loadColor(float red, float green, float blue) {
		loadColor(red, green, blue, 1.0f);
	}

	public void loadColor(float red, float green, float blue, float alpha) {
		super.loadVector(location_color, red, green, blue, alpha);
	}

	public void loadTextureOffset(Vector4f vec) {
		loadTextureOffset(vec.x, vec.y, vec.z, vec.w);
	}

	public void loadTextureOffset(float x, float y, float z, float w) {
		super.loadVector(location_textureOffset, x, y, z, w);
	}

	public void loadTransformationMatrix(Matrix4f matrix) {
		super.loadMatrix(location_transformationMatrix, matrix);
	}

	public void loadProjectionMatrix(Matrix4f matrix) {
		super.loadMatrix(location_projectionMatrix, matrix);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "vertexPos");
		super.bindAttribute(1, "textureCoords");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_textureOffset = super.getUniformLocation("textureOffset");
		location_color = super.getUniformLocation("color");
	}

	@Override
	public void start() {
		super.start();
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
	}

	@Override
	public void stop() {
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		super.stop();
	}
}

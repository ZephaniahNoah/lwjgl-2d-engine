package com.zephaniahnoah.lwjgl2D.gfx.shaders;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import com.zephaniahnoah.lwjgl2D.Log;
import com.zephaniahnoah.lwjgl2D.StaticLog;

public class Shader implements Log {

	private String vertex;
	private String fragment;
	private int programID, vertexID, fragmentID;
	private Map<String, byte[]> digests = new HashMap<String, byte[]>();
	private static final String vertexExtension = ".vert";
	private static final String fragmentExtension = ".frag";
	private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

	public Shader(String name) {
		this(name, name);
	}

	public Shader(String vertex, String fragment) {
		this.vertex = vertex;
		this.fragment = fragment;
		create();
	}

	public void create() {
		programID = glCreateProgram();

		vertexID = loadShader(vertex, GL_VERTEX_SHADER);
		fragmentID = loadShader(fragment, GL_FRAGMENT_SHADER);

		glAttachShader(programID, vertexID);
		glAttachShader(programID, fragmentID);

		bindAttributes();

		glLinkProgram(programID);
		glValidateProgram(programID);

		getAllUniformLocations();

		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
	}

	private int loadShader(String file, int type) {
		int id = glCreateShader(type);
		String fileName = "shaders/" + file + getExtension(type);
		glShaderSource(id, readFile(fileName, type));
		glCompileShader(id);

		if (glGetShaderi(id, GL_COMPILE_STATUS) == GL_FALSE) {
			log("Failed to compile shader " + fileName);
			log(glGetShaderInfoLog(id));
		}

		return id;
	}

	private String readFile(String file, int type) {
		try (InputStream stream = getClass().getResourceAsStream("/" + file)) {

			// Save checksum of file
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] data = new byte[stream.available()];
			stream.read(data);
			digests.put(file, md.digest(data));

			BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)));
			String shader = "";

			while (reader.ready()) {
				shader += reader.readLine() + "\n";
			}

			return shader;
		} catch (Exception e) {
			log("Failed to load shader " + file);
			e.printStackTrace();
		}

		return null;
	}

	private String getExtension(int type) {
		if (type == GL_VERTEX_SHADER)
			return vertexExtension;
		return fragmentExtension;
	}

	public void delete() {
		glDetachShader(programID, vertexID);
		glDetachShader(programID, fragmentID);
		glDeleteProgram(programID);
	}

	protected void getAllUniformLocations() {
	}

	protected int getUniformLocation(String name) {
		return glGetUniformLocation(programID, name);
	}

	protected void bindAttributes() {
	}

	protected void bindAttribute(int attribute, String name) {
		glBindAttribLocation(programID, attribute, name);
	}

	protected void loadFloat(int location, float value) {
		glUniform1f(location, value);
	}

	protected void loadVector(int location, float x, float y) {
		glUniform2f(location, x, y);
	}

	protected void loadVector(int location, float x, float y, float z) {
		glUniform3f(location, x, y, z);
	}

	protected void loadVector(int location, float x, float y, float z, float w) {
		glUniform4f(location, x, y, z, w);
	}

	protected void loadMatrix(int location, Matrix4f matrix) {
		store(matrix);
		matrixBuffer.flip();
		glUniformMatrix4fv(location, false, matrixBuffer);
	}

	protected void loadInt(int location, int value) {
		glUniform1i(location, value);
	}

	public void store(Matrix4f matrix) {
		matrixBuffer.put(matrix.m00());
		matrixBuffer.put(matrix.m01());
		matrixBuffer.put(matrix.m02());
		matrixBuffer.put(matrix.m03());
		matrixBuffer.put(matrix.m10());
		matrixBuffer.put(matrix.m11());
		matrixBuffer.put(matrix.m12());
		matrixBuffer.put(matrix.m13());
		matrixBuffer.put(matrix.m20());
		matrixBuffer.put(matrix.m21());
		matrixBuffer.put(matrix.m22());
		matrixBuffer.put(matrix.m23());
		matrixBuffer.put(matrix.m30());
		matrixBuffer.put(matrix.m31());
		matrixBuffer.put(matrix.m32());
		matrixBuffer.put(matrix.m33());
	}

	public boolean hotSwap() {
		for (Entry<String, byte[]> digest : digests.entrySet()) {
			try (InputStream stream = getClass().getResourceAsStream("/" + digest.getKey())) {
				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] data = new byte[stream.available()];
				stream.read(data);
				byte[] md5 = md.digest(data);

				if (!Arrays.equals(digest.getValue(), md5)) {
					delete();
					create();
					log("Reloading " + digest.getKey());
					return true;
				}
			} catch (Exception e) {
				StaticLog.exception(e);
			}
		}
		return false;
	}

	public void start() {
		glUseProgram(programID);
	}

	public void stop() {
		glUseProgram(0);
	}
}
package com.zephaniahnoah.lwjgl2D.gfx;

import org.joml.Math;
import org.joml.Vector2d;
import org.joml.Vector2i;

public class Camera {

	public static Vector2d position = new Vector2d();
	public static Vector2i positionI = new Vector2i();
	public static float lerp = 0.1f;
	public static double fallowX = 0;
	public static double fallowY = 0;

	public static void update(float delta) {
		position.x += ((fallowX - position.x) * lerp) * delta;
		position.y += ((fallowY - position.y) * lerp) * delta;
		positionI.set((int) Math.round(position.x), (int) Math.round(position.y));
	}
}
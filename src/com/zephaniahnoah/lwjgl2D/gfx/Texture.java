package com.zephaniahnoah.lwjgl2D.gfx;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.stb.STBImage.*;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

import com.zephaniahnoah.lwjgl2D.Log;
import com.zephaniahnoah.lwjgl2D.gfx.shaders.TexturedShader;

public class Texture implements Log {

	private static int vertexCount;
	public static List<Texture> textures = new ArrayList<Texture>();
	public int textureID, width, height;
	public ByteBuffer buffer;
	private static List<Integer> vboList = new ArrayList<Integer>();
	public boolean hasAlpha = false;
	private static final ByteBuffer missing = buffer(32, 32, Color.MAGENTA);

	public Texture() {
	}

	public Texture(int width, int height, Color c) {
		this.width = width;
		this.height = height;
		buffer = buffer(width, height, c);
		textureID = glGenTextures();
		postInit(GL_NEAREST, GL_LINEAR);
	}

	public Texture(String name) {
		this(name, GL_NEAREST);
	}

	public Texture(String name, float filter) {
		this(name, filter, GL_LINEAR);
	}

	public Texture(String name, float filter, float minFilter) {
		String directory = "/textures/" + name + ".png";
		init(getClass().getResourceAsStream(directory), directory, filter, minFilter);
	}

	public Texture(URL url) {
		this(url, GL_NEAREST);
	}

	public Texture(URL url, float filter) {
		try {
			URLConnection con = url.openConnection();

			// con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			init(con.getInputStream(), url.toString(), filter, GL_LINEAR);
		} catch (IOException e) {
			log("Failed to download image from " + url);
			e.printStackTrace();
		}
	}

	public void init(InputStream stream, String location, float filter, float minFilter) {
		try (InputStream in = getClass().getResourceAsStream(location)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int byteRead = 0;

			while (byteRead != -1) {
				byteRead = in.read();
				baos.write(byteRead);
			}

			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(baos.size());
			byteBuffer.put(baos.toByteArray());

			byteBuffer.flip();

			IntBuffer w = BufferUtils.createIntBuffer(1);
			IntBuffer h = BufferUtils.createIntBuffer(1);
			IntBuffer channels = BufferUtils.createIntBuffer(1);

			ByteBuffer image = stbi_load_from_memory(byteBuffer, w, h, channels, 0);

			if (image == null) {
				log("Failed to parse texture: " + location);
				initBlankImage(filter, minFilter);
			} else {
				width = w.get();
				height = h.get();

				buffer = image;
				hasAlpha = channels.get() > 3;

				textureID = glGenTextures();

				postInit(filter, minFilter);
			}
		} catch (Exception e) {
			log("Failed to load texture: " + location);
			log("Reason: " + e.getMessage());
			e.printStackTrace();

			initBlankImage(filter, minFilter);
		}
	}

	private void initBlankImage(float filter, float minFilter) {
		width = 32;
		height = 32;
		buffer = missing;
		textureID = glGenTextures();
		postInit(filter, minFilter);
	}

	private static ByteBuffer buffer(int width, int height, Color c) {
		ByteBuffer buffer = ByteBuffer.allocateDirect(4 * width * height);

		for (int i = 0; i < width * height; i++) {
			buffer.put((byte) c.getRed()).put((byte) c.getGreen()).put((byte) c.getBlue()).put((byte) c.getAlpha());
		}

		return (ByteBuffer) buffer.flip();
	}

	public void postInit(float filter, float minFilter) {
		textures.add(this);

		glBindTexture(GL_TEXTURE_2D, textureID);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	}

	public static void initBufferObjects() {
		float[] vertices = { //
				-0.5f, +0.5f, 0f, // v0
				-0.5f, -0.5f, 0f, // v1
				+0.5f, -0.5f, 0f, // v2
				+0.5f, +0.5f, 0f, // v3
		};

		int[] indices = { //
				0, 1, 3, // top left triangle (v0, v1, v3)
				3, 1, 2, // bottom right triangle (v3, v1, v2)
		};

		float[] tex_coords = new float[] { //
				0, 0, // V0
				0, 1, // V1
				1, 1, // V2
				1, 0, // V3
		};

		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 3, vertices);
		storeDataInAttributeList(1, 2, tex_coords);
		vertexCount = indices.length;
	}

	private static void bindIndicesBuffer(int[] indicies) {
		int vboID = glGenBuffers();
		vboList.add(vboID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies, GL_STATIC_DRAW);
	}

	private static void storeDataInAttributeList(int attributeNumber, int coord, float[] data) {
		int vboID = glGenBuffers();
		vboList.add(vboID);
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW);
		glVertexAttribPointer(attributeNumber, coord, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	public void delete() {
		glDeleteTextures(textureID);
		buffer.clear();
		textures.remove(this);
	}

	public static void cleanUp() {
		deleteAll();
		for (int vbo : vboList)
			glDeleteBuffers(vbo);
	}

	public static void deleteAll() {
		for (Texture texture : new ArrayList<>(textures)) {
			texture.delete();
		}
	}

	public void draw(TexturedShader s, int offsetStartX, int offsetStartY, int offsetEndX, int offsetEndY) {
		s.loadTextureOffset(offsetStartX, offsetStartY, offsetEndX, offsetEndY);
		draw();
	}

	public void bindAndDraw(TexturedShader s) {
		bindAndDraw(s, 0, 0, width, height);
	}

	public void bindAndDraw(TexturedShader s, int offsetStartX, int offsetStartY, int offsetEndX, int offsetEndY) {
		glBindTexture(GL_TEXTURE_2D, textureID);

		if (hasAlpha)
			Graphics.enableBlending();

		draw(s, offsetStartX, offsetStartY, offsetEndX, offsetEndY);

		if (hasAlpha)
			Graphics.disableBlending();
	}

	public void draw(TexturedShader s) {
		draw(s, 0, 0, width, height);
	}

	public static void draw() {
		glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);
	}
}

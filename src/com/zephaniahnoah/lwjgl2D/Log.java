package com.zephaniahnoah.lwjgl2D;

public interface Log {

	public default void log(Object message) {
		log(message, getClass().getSimpleName());
	}

	public default void log(Object message, String name) {
		invis("[" + name + "] " + message);
	}

	public default void invis(Object message) {
		StaticLog.invis(message);
	}
}
package com.zephaniahnoah.lwjgl2D;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.stb.STBVorbis.*;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

public class Sound implements Log {

	private int bufferID;
	private List<Integer> sourceIDs = new ArrayList<Integer>();
	public String file;
	private boolean looping;

	public Sound(String name) {
		this(name, false);
	}

	public Sound(String name, boolean looping) {
		this.file = "/sounds/" + name + ".ogg";
		this.looping = looping;

		MemoryStack.stackPush();
		IntBuffer channelsBuffer = MemoryStack.stackMallocInt(1);
		MemoryStack.stackPush();
		IntBuffer sampleRateBuffer = MemoryStack.stackMallocInt(1);

		try (InputStream in = getClass().getResourceAsStream(file)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int byteRead = 0;

			while (byteRead != -1) {
				byteRead = in.read();
				baos.write(byteRead);
			}

			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(baos.size());
			byteBuffer.put(baos.toByteArray());

			byteBuffer.flip();

			ShortBuffer rawAudioBuffer = stb_vorbis_decode_memory(byteBuffer, channelsBuffer, sampleRateBuffer);

			if (rawAudioBuffer == null) {
				log("Failed to decode sound: " + file);
				MemoryStack.stackPop();
				MemoryStack.stackPop();
				return;
			}

			int channels = channelsBuffer.get();
			int sampleRate = sampleRateBuffer.get();

			MemoryStack.stackPop();
			MemoryStack.stackPop();

			int format = -1;

			if (channels == 1) {
				format = AL_FORMAT_MONO16;
			} else if (channels == 2) {
				format = AL_FORMAT_STEREO16;
			}

			bufferID = alGenBuffers();

			alBufferData(bufferID, format, rawAudioBuffer, sampleRate);

			MemoryUtil.memFree(rawAudioBuffer);

			return;
		} catch (Exception e) {
			log("Failed to load sound: " + file);
			StaticLog.exception(e);
		}

		MemoryStack.stackPop();
		MemoryStack.stackPop();
	}

	public void delete(int sourceID) {
		alDeleteSources(sourceID);
		sourceIDs.remove(sourceID);
	}

	public void deleteStopped() {
		ArrayList<Integer> deleted = new ArrayList<Integer>();
		for (int id : sourceIDs) {
			if (alGetSourcei(id, AL_SOURCE_STATE) == AL_STOPPED) {
				alDeleteSources(id);
				deleted.add(id);
			}
		}
		sourceIDs.removeAll(deleted);
		deleted.clear();
	}

	public void deleteEverything() {
		alDeleteBuffers(bufferID);
		for (int id : sourceIDs) {
			alDeleteSources(id);
		}
		sourceIDs.clear();
	}

	public void setVolume(float gain) {
		if (isPlaying()) {
			for (int id : sourceIDs) {
				alSourcef(id, AL_GAIN, gain);
			}
		}
	}

	public void play(int sourceID, float gain) {
		alSourcei(sourceID, AL_BUFFER, bufferID);
		alSourcei(sourceID, AL_LOOPING, looping ? 1 : 0);
		alSourcei(sourceID, AL_POSITION, 0);
		alSourcef(sourceID, AL_GAIN, gain);

		int state = alGetSourcei(sourceID, AL_SOURCE_STATE);

		boolean isPlaying = !(state == AL_STOPPED);

		if (state == AL_STOPPED) {
			isPlaying = false;
			alSourcei(sourceID, AL_POSITION, 0);
		}

		if (isPlaying)
			stop(sourceID);

		alSourcePlay(sourceID);
	}

	public void play() {
		play(0.3f);
	}

	public void play(float gain) {

		int sourceID;

		if (looping && sourceIDs.size() > 0) {
			sourceID = sourceIDs.get(0);
		} else {
			sourceID = alGenSources();
			sourceIDs.add(sourceID);
		}

		play(sourceID, gain);
		deleteStopped();
	}

	public void stopAll() {
		for (int id : sourceIDs)
			stop(id);
	}

	public void stop(int sourceID) {
		int state = alGetSourcei(sourceID, AL_SOURCE_STATE);
		boolean isPlaying = !(state == AL_STOPPED);

		if (isPlaying)
			alSourceStop(sourceID);
	}

	public boolean isPlaying() {
		if (sourceIDs.size() > 0) {
			int sourceID = sourceIDs.get(0);
			int state = alGetSourcei(sourceID, AL_SOURCE_STATE);
			return !(state == AL_STOPPED);
		} else {
			return false;
		}
	}

	public boolean isPlaying(int sourceID) {
		int state = alGetSourcei(sourceID, AL_SOURCE_STATE);
		return !(state == AL_STOPPED);
	}
}

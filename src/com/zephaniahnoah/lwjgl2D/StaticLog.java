package com.zephaniahnoah.lwjgl2D;

import java.io.File;
import java.io.FileWriter;

public class StaticLog {

	private static File logFile = new File("log.txt");
	private static FileWriter writer;
	private static String lastMessage = "";
	private static int count = 0;
	private static String messageBuffer = "";

	static {
		if (logFile.exists()) {
			logFile.delete();
		}

		try {
			logFile.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void log(Object message) {
		log(message, "Unknown");
	}

	public static void log(Object message, String name) {
		invis("[" + name + "] " + message);
	}

	public static void exception(Exception e) {
		e.printStackTrace();
		write("[" + e.getClass().getSimpleName() + "] " + e.getMessage() + "\n");
		for (StackTraceElement element : e.getStackTrace()) {
			write("    " + element + "\n");
		}
	}

	public static void invis(Object message) {
		if (lastMessage.equals(message.toString())) {
			// System.out.println(message);
			count++;
			return;
		} else {
			int lastCount = count;
			if (count > 1) {
				count = 0;
				invis("x" + lastCount + " " + lastMessage);
			}
			count = 0;
			System.out.println(message);
		}

		lastMessage = message.toString();
		messageBuffer += message + "\n";
	}

	public static void write() {
		String message = messageBuffer;
		messageBuffer = "";
		write(message);
	}

	public static void write(String message) {
		try {
			writer = new FileWriter(logFile, true);
			writer.write(message);
			// System.out.print(message);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

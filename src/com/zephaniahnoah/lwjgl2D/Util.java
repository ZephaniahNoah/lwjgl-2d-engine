package com.zephaniahnoah.lwjgl2D;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.joml.Vector2d;

public class Util {

	public static boolean roughEquals(Vector2d v1, Vector2d v2) {
		return round9Places(v1.x) == round9Places(v2.x) && round9Places(v1.y) == round9Places(v2.y);
	}

	public static double round9Places(double d) {
		return Math.round(d *= 1000000000) / 1000000000;
	}

	public static boolean near(Vector2d a, Vector2d b, double margin) {
		return near(a.x, a.y, b.x, b.y, margin);
	}

	public static boolean near(double x1, double y1, double x2, double y2, double margin) {
		return near(x1, y1, 0, x2, y2, 0, margin);
	}

	public static boolean near(double x1, double y1, double z1, double x2, double y2, double z2, double margin) {
		return near(x1, x2, margin) && near(y1, y2, margin) && near(z1, z2, margin);
	}

	private static boolean near(double x1, double y1, double margin) {
		return x1 + margin > y1 && x1 - margin < y1;
	}

	public static Set<Class<?>> getClassesInPackage(String packageName) {

		String path = packageName.replace(".", "/");
		Set<Class<?>> classes = new HashSet<>();
		String[] classPathEntries = System.getProperty("java.class.path").split(System.getProperty("path.separator"));

		String name;
		for (String classpathEntry : classPathEntries) {
			if (classpathEntry.endsWith(".jar")) {
				File jar = new File(classpathEntry);
				try (JarInputStream is = new JarInputStream(new FileInputStream(jar))) {
					JarEntry entry;
					while ((entry = is.getNextJarEntry()) != null) {
						name = entry.getName();
						if (name.endsWith(".class")) {
							if (name.contains(path) && name.endsWith(".class")) {
								String classPath = name.substring(0, entry.getName().length() - 6);
								classPath = classPath.replaceAll("[\\|/]", ".");
								classes.add(Class.forName(classPath));
							}
						}
					}
				} catch (Exception e) {
					StaticLog.exception(e);
				}
			} else {
				try {
					File base = new File(classpathEntry + File.separatorChar + path);
					for (File file : base.listFiles()) {
						name = file.getName();
						if (name.endsWith(".class")) {
							name = name.substring(0, name.length() - 6);
							classes.add(Class.forName(packageName + "." + name));
						}
					}
				} catch (Exception e) {
					// StaticLog.exception(e);
				}
			}
		}

		return classes;
	}
}

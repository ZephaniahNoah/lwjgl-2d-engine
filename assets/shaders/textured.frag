#version 120
#extension GL_EXT_gpu_shader4 : enable

varying vec2 tex_coords;

uniform sampler2D tex;
uniform vec4 textureOffset;
uniform vec4 color;

void main(void) {
	ivec2 texSize = textureSize2D(tex, 0);

	float width = float(texSize.x);
	float height = float(texSize.y);

	float startX = textureOffset.x / width;
	float startY = textureOffset.y / height;

	float endX = (textureOffset.z - textureOffset.x) / width;
	float endY = (textureOffset.w - textureOffset.y) / height;

	gl_FragColor = texture2D(tex, vec2( //
			(startX + (tex_coords.x) * endX), //
			(startY + (tex_coords.y) * endY)));

	gl_FragColor *= color;
}

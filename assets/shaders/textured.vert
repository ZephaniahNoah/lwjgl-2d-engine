#version 120

attribute vec3 vertexPos;
attribute vec2 textureCoords;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;

varying vec2 tex_coords;

void main(void) {
	tex_coords = textureCoords;
	gl_Position = projectionMatrix * transformationMatrix * vec4((vertexPos), 1.0);
}

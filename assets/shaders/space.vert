#version 400 core

in vec3 vertexPos;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;

void main(void) {
	gl_Position = projectionMatrix * transformationMatrix * vec4((vertexPos), 1.0);
}
